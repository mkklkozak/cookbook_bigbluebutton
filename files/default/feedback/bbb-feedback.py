#!/usr/bin/python3
import sys
import json
import psycopg2
from datetime import datetime
with open("/etc/bigbluebutton/feedback.connstring", "r") as f:
    connstring = f.readline()
conn = psycopg2.connect(connstring)
cur = conn.cursor()
cur.execute("""
    CREATE TABLE IF NOT EXISTS feedback (
        id serial primary key, 
        hostname varchar(255) not null,
        ts timestamp not null, 
        userName varchar(255),
        comment text,
        userRole varchar(255),
        meetingId varchar(255),
        userId varchar(255),
        authToken varchar(255),
        rating integer
    )
""")
conn.commit()
months = {
    'Jan': 1,
    'Feb': 2,
    'Mar': 3,
    'Apr': 4,
    'May': 5,
    'Jun': 6,
    'Jul': 7,
    'Aug': 8,
    'Sep': 9,
    'Oct': 10,
    'Nov': 11,
    'Dec': 12,
}
# {'userName': '[unconfirmed] XXXX YYYY', 'comment': '', 'userRole': 'MODERATOR', 'meetingId': '3aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa-bbbbbbbbbbbbb', 'userId': 'a_bbbbbbbbbbbb', 'authToken': 'cccccccccccc', 'rating': 5}
while True:
    line = sys.stdin.readline()
    key = "info: FEEDBACK LOG: "
    try:
        idx = line.index(key)
        data = json.loads(line[idx+len(key):])
        print(line[0:idx])
        mon, day, time, hostname = line[0:idx].split(" ")[0:4]
        h,m,s = time.split(":")
        ts = datetime(datetime.now().year, months[mon], int(day), int(h), int(m), int(s))
        print(data)
        cur.execute("INSERT INTO feedback (hostname, ts, username, comment, userRole, meetingId, userId, authToken, rating) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
            (hostname, ts, data['userName'], data['comment'], data['userRole'], data['meetingId'], data['userId'], data['authToken'], data['rating'])
        )
        conn.commit()
    except ValueError:
        pass

