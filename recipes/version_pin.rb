# this recipe installs a version pin file for apt, so packages will be installed in the desired versions

if node['bigbluebutton']['version_pin']
  begin
    cookbook = node.run_context.cookbook_collection['bigbluebutton']
    file_cache_location = cookbook.preferred_filename_on_disk_location(run_context.node, :files, "package-pin/#{node['bigbluebutton']['version_pin']}.json")

    template '/etc/apt/preferences.d/bbb.pref' do
      source 'packaging/bbb.pref.erb'
      variables versions: JSON.load(File.read(file_cache_location))
      only_if { ::File.exist?(file_cache_location) }
    end
  rescue Chef::Exceptions::FileNotFound
    log "Version data not found. Check that files/*/package-pin/#{node['bigbluebutton']['version_pin']}.json exists in cookbook" do
      level :error
    end
  end
end
