package %w(docker-ce docker-ce-cli containerd.io)

service 'docker' do
  action [:enable, :start]
end

package 'ruby'
gem_package 'bundler' do
  version '2.1.4'
  gem_binary '/usr/bin/gem'
end

package %w(nodejs mongodb-org haveged build-essential yq)

user 'redis' do
  system true
  home '/var/lib/redis'
  shell '/usr/sbin/nologin'
end

directory '/etc/redis' do
  user 'redis'
end

cookbook_file '/etc/redis/redis.conf' do
  source 'etc/redis/redis.conf'
  owner 'redis'
  mode '0600'
end

package 'redis'

service 'redis-server' do
  action [:enable, :start]
end
