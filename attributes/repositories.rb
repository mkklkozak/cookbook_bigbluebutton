default['bigbluebutton']['repo']['rmescandon-ubuntu-yq-bionic'] = {
  uri: 'ppa:rmescandon/yq',
  key: 'CC86BB64',
  components: ['main'],
}
default['bigbluebutton']['repo']['libreoffice-ubuntu-ppa-bionic'] = {
  uri: 'ppa:libreoffice/ppa',
  key: '1378B444',
  components: ['main'],
}
default['bigbluebutton']['repo']['bigbluebutton-ubuntu-support-bionic'] = {
  uri: 'ppa:bigbluebutton/support',
  key: 'E95B94BC',
  components: ['main'],
}
default['bigbluebutton']['repo']['kurento'] = {
  uri: 'http://ubuntu.openvidu.io/6.16.0',
  key: '5AFA7A83',
  components: ['kms6'],
  arch: 'amd64',
}
default['bigbluebutton']['repo']['nodesource'] = {
  uri: 'https://deb.nodesource.com/node_12.x',
  key: 'https://deb.nodesource.com/gpgkey/nodesource.gpg.key',
  components: ['main'],
}
default['bigbluebutton']['repo']['mongodb-org-4.2'] = {
  uri: 'https://repo.mongodb.org/apt/ubuntu',
  key: 'https://www.mongodb.org/static/pgp/server-4.2.asc',
  components: ['multiverse'],
  distribution: 'bionic/mongodb-org/4.2',
}
default['bigbluebutton']['repo']['docker-ce'] = {
  uri: 'https://download.docker.com/linux/ubuntu',
  key: 'https://download.docker.com/linux/ubuntu/gpg',
  components: ['stable'],
  arch: 'amd64',
}
default['bigbluebutton']['repo']['bigbluebutton'] = {
  uri: 'https://ubuntu.bigbluebutton.org/bionic-230',
  key: 'https://ubuntu.bigbluebutton.org/repo/bigbluebutton.asc',
  components: ['main'],
  distribution: 'bigbluebutton-bionic',
}
